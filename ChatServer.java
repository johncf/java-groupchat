import java.io.*;
import java.net.*;
import java.util.Vector;

public class ChatServer {
    static Vector<ClientHandler> clients = new Vector<>();
    static final int port = 9000;

    public static void main(String args[]) throws IOException {
        ServerSocket server;
        System.out.println("Binding to port " + port);
        server = new ServerSocket(port);
        System.out.println("Server bound. Waiting for clients...");
        while (true) {
            Socket client = server.accept();
            System.out.println("Client accepted: " + client);
            ClientHandler ch = new ClientHandler(client, clients);
            Thread t = new Thread(ch);
            clients.add(ch);
            t.start();
        }
    }
}
