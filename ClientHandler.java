import java.io.*;
import java.net.Socket;
import java.util.Vector;

public class ClientHandler implements Runnable {
    private Socket sock;
    private BufferedReader in;
    PrintWriter out;
    Vector<ClientHandler> clist;

    public ClientHandler(Socket client, Vector<ClientHandler> clist) throws IOException {
        this.sock = client;
        this.in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.out = new PrintWriter(client.getOutputStream(), true);
        this.clist = clist;
    }

    @Override
    public void run() {
        while (true) {
            String received;
            try {
                received = this.in.readLine();
                if (received == null) break;
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
            for (ClientHandler ch : clist) {
                if (ch != this) {
                    ch.out.println(received);
                }
            }
        }
        System.out.println("Client left: " + this.sock);
        clist.remove(this);
        try {
            this.sock.close();
        } catch (IOException e) {}
    }
}
