import java.net.*;
import java.io.*;

public class ChatClient implements Runnable {
    public static void main(String args[]) throws IOException {
        System.out.println("Connecting...");
        Socket server = new Socket("localhost", 9000);
        PrintWriter out = new PrintWriter(server.getOutputStream(), true);
        ChatClient cc = new ChatClient(server.getInputStream());
        System.out.println("Connected to server.");

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter your nick: ");
        String nick = console.readLine();
        System.out.println("You may now start sending messages...");

        Thread t = new Thread(cc);
        t.start();

        while (true) {
            String message = console.readLine();
            if (message.isEmpty()) continue;
            out.println(nick + ": " + message);
        }
    }

    private BufferedReader in;

    public ChatClient(InputStream in) {
        this.in = new BufferedReader(new InputStreamReader(in));
    }

    @Override
    public void run() {
        while (true) {
            try {
                String received = this.in.readLine();
                if (received == null) {
                    System.err.println("Server says bye!");
                    System.exit(0);
                }
                System.out.println(received);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
